# Events

## Workshop on Generic Structures, 24-30 October 2021
The workshop is devoted to recent results and current problems around the theory of generic structures. The aim is to gather specialists working in the area as well as students and young researchers interested in the topic.

<https://gens.math.cas.cz/>

Organizers: Tristan Bice, Wiesław Kubiś, Christian Pech, Beata Kubiś (project manager).




## Bohemian Logical and Philosophical Café

In 2020, the project supported the Bohemian Logical & Philosophical Café, a framework for webinars in emerging topics in philosophy and logic. The project has a quite broad scope and is willing to encompass a variety of subjects, ranging from theoretical computer science to philosophy.

<https://bohemianlpc.github.io>

Coordinator: Ivan Di Liberti. Scientific Committee: Paul-André Mellies, Thomas Streicher, Steve Awodey, Ivan Di Liberti, Mirna Džamonja, Wiesław Kubiś, Jean-Pierre Marquis, David Corfield.


## Course in Category Theory

In Winter 2020, the project hosted a course in Category Theory. The course will took place at the Institute of Mathematics of the Czech Academy of Sciences in the blue lecture room every Tuesday from 15 to 17 o'clock.
The course was open to bachelor, master students and researchers that are not familiar with the topic. 

<https://diliberti.github.io/Teaching/Teaching%20CAS/CT/2020/CT2020.html>

Lecturer: Ivan Di Liberti.
