<style>
</style>

<div class="info">


<div>

### [Stevo Todorčević](https://www.math.toronto.edu/cms/people/faculty/todorcevic-stevo/)

*University of Toronto / Institut de mathématiques de Jussieu / Mathematical Institute, SASA*

research interests: Set theory and combinatorics

workshop: 24.10 - 30.10 2021

lecture: Forcing with Copies of the Rado and Hensen Graphs

[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Todorcevic%2C+S)
</div>




<div>

### [Paul Szeptycki](https://szeptycki.info.yorku.ca)

*York University*

research interests: General topology, Descriptive set theory

workshop: 24.10 - 30.10 2021

lecture: A topological space from a \\ \square (\kappa) \\ sequence related to convergence and cardinal invariants of the \\ G_\delta \\ topology

[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Szeptycki%2C+P)
</div>



<div>

### [Marcin Sabok](https://www.mcgill.ca/mathstat/marcin-sabok)

*McGill University*

research interests: Descriptive set theory, Ramsey theory and forcing. Recently, he is also interested in applications and connections of logic with fields such as operator algebras and geometric group theory

workshop: 24.10 - 30.10 2021

lecture: Perfect matchings in hyperfinite graphings

[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Sabok%2C+M)
</div>



<div>

### [Maja Pech](https://people.dmi.uns.ac.rs/~maja/)

*University of Novi Sad*

research interests: Universal Algebra

workshop: 24.10 - 30.10 2021

lecture: Classification methods for homomorphism-homogeneous structures

[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Pech%2C+M)
</div>



<div>

### [Dragan Mašulović](https://personal.pmf.uns.ac.rs/masul/)

*University of Novi Sad*

research interests:  Universal Algebra, Ramsey theory

workshop: 24.10 - 30.10 2021

lecture: On big (dual) Ramsey degrees in free algebras and cofree coalgebras.

[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Masulovic%2C+D)
</div>



<div>

### [Maciej Malicki](https://ssl-kolegia.sgh.waw.pl/pl/KAE/struktura/KMiEM/katedra/sklad/Strony/malicki.aspx)

*Institute of Mathematics, Polish Academy of Sciences*

research interests: Fraïssé theory,  automorphism groups, Polish groups

workshop: 24.10 - 30.10 2021

lecture: Large conjugacy classes and weak amalgamation


[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Malicki%2C+M)
</div>

<div>

### [Aleksandra Kwiatkowska](https://sites.google.com/site/akwiatkmath/)

*University of Münster / University of Wrocław*

research interests:  Fraïssé theory, topological dynamics, automorphism groups

workshop: 24.10 - 30.10 2021

lecture: Simplicity of the automorphism groups of ultrahomogeneous structures

[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Kwiatkowska%2C+A)
</div>


<div>

### [Boriša Kuzeljević](https://people.dmi.uns.ac.rs/~borisha/)

*University of Novi Sad*

research interests: ultrahomogeneous structures, infininary combinatorics of hypergraph theory

workshop: 24.10 - 30.10 2021

lecture: Tukey order of directed sets of cofinality  \\ \omega_2 \\

[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Kuzeljevic%2C+B)
</div>





<div>

### [Franz-Viktor Kuhlmann](https://math.usask.ca/~fvk/)

*University of Szczecin*

research interests: valuation theory, real algebra and ordered structures, model theoretic algebra

workshop: 24.10 - 30.10 2021

lecture: About hyperfields

[ArXiv Profile](https://arxiv.org/search/?searchtype=author&query=Kuhlmann%2C+F)
</div>



<div>

### [Vadim Alekseev](http://v-alekseev.org/)

*Technische Universität Dresden, Institut für Geometrie*

research interests: Operator algebras, L2-invariants, Geometric and
ergodic group theory, Noncommutative geometry

visit to Prague team: 18.2. - 28.2. 2020 
workshop: 24.10 - 30.10 2021

invited lecture: [Sofic approximations, coarse geometry and operator algebras](https://calendar.math.cas.cz/content/sofic-approximations-coarse-geometry-and-operator-algebras)

[ArXiv Profile](https://arxiv.org/a/alekseev_v_1.html)
</div>

<div>

### [Mirna Džamonja](https://www.logiqueconsult.eu)

*CNRS- Université Panthéon-Sorbonne, Institut d’Histoire et de la Philosophie des Sciences et des Techniques IHPST, France*


research interest: Logic in all its aspects : mathematical, philosophical and in computer sciences

visit to Prague team: 14.9. - 18.9. 2020
workshop: 24.10 - 30.10 2021

invited lecture: [On logics that make a bridge from the Discrete to the Continuous](https://calendar.math.cas.cz/content/logics-make-bridge-discrete-continuous)

[ArXiv Profile](https://arxiv.org/search/math?searchtype=author&query=Džamonja,+M)
</div>

</div>
